const request = require('supertest');
const { app, baseUrl } = require('../app/app.js');


describe('Register Endpoint', () => {
  it('should register a new user', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/registration')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: 'john8',
        password: 'password123',
        email: 'john8@example.com',
        name: 'John Doe'
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('message', 'Success retrieve data');
  });

  it('register a new user invalid with same username & Email', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/registration')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: 'john8',
        password: 'password123',
        email: 'john8@example.com',
        name: 'John Doe'
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(500);
    expect(response.body).toHaveProperty('error.errors', ['Validation failed: Username has already been taken, Email has already been taken']);
  });
  
});
