const request = require('supertest');
const { app, baseUrl } = require('../app/app.js');


describe('Login Endpoint', () => {

  let testToken;

  it('user login with valid data', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/auth')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: 'ari',
        password: 'password123',
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('message', 'Success retrieve data');
    expect(response.body).toHaveProperty('data');
    expect(response.body.data).toHaveProperty('access_token');
    expect(response.body.data.access_token).toHaveProperty('token', response.body.data.access_token.token);
    expect(response.body.data.access_token).toHaveProperty('type', 'Bearer');
    expect(response.body.data.access_token).toHaveProperty('expires_at', response.body.data.access_token.expires_at);

    testToken = response.body.data.access_token.token;
  });

  it('user update profile', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');
    

    const response = await request(baseUrl)
      .post('/profile/update')
      .set('Authorization', `Bearer ${testToken}`)
      .send({
        username: 'ari',
        password: 'password123',
        email: 'john8@example.com',
        name: 'mrjhon',
        phone: '081219012922',
        gender: 'male',
        address: 'yk'
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('message', 'Success retrieve data');
    expect(response.body).toHaveProperty('data');
    expect(response.body.data).toHaveProperty('user');
    expect(response.body.data.user).toHaveProperty('username', response.body.data.user.username);
    expect(response.body.data.user).toHaveProperty('name', response.body.data.user.name);
    expect(response.body.data.user).toHaveProperty('email', response.body.data.user.email);
    // expect(response.body.data.user).toHaveProperty('password', response.body.data.user.password);
    expect(response.body.data.user).toHaveProperty('phone', response.body.data.user.phone);
    expect(response.body.data.user).toHaveProperty('gender', response.body.data.user.gender);
    expect(response.body.data.user).toHaveProperty('address', response.body.data.user.address);

  });


  it('user get profile', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');
    

    const response = await request(baseUrl)
      .get('/profile')
      .set('Authorization', `Bearer ${testToken}`)

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('message', 'Success retrieve data');
    expect(response.body).toHaveProperty('data');
    expect(response.body.data).toHaveProperty('user');
    expect(response.body.data.user).toHaveProperty('username', response.body.data.user.username);
    expect(response.body.data.user).toHaveProperty('name', response.body.data.user.name);
    expect(response.body.data.user).toHaveProperty('email', response.body.data.user.email);

  });

  // it('user login with empty data', async () => {
  //   const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

  //   const response = await request(baseUrl)
  //     .post('/auth')
  //     .set('Authorization', `Basic ${credentials}`)
  //     .send({
  //       username: '',
  //       password: '',
  //     });

  //   console.log('Response status:', response.statusCode);
  //   console.log('Response body:', response.body);

  //   expect(response.statusCode).toBe(400);
  //   expect(response.body).toHaveProperty('error.errors', ['Invalid username / password combination']);
  // });
  
});
