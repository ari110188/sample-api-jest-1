const request = require('supertest');
const { app, baseUrl } = require('../app/app.js');


describe('Login Endpoint', () => {
  it('user login with valid data', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/auth')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: 'john8',
        password: 'password123',
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('message', 'Success retrieve data');
  });

  it('user login with invalid data', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/auth')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: 'john50',
        password: 'password123',
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(400);
    expect(response.body).toHaveProperty('error.errors', ['Invalid username / password combination']);
  });

  it('user login with empty data', async () => {
    const credentials = Buffer.from('tahubulat:enaksekali').toString('base64');

    const response = await request(baseUrl)
      .post('/auth')
      .set('Authorization', `Basic ${credentials}`)
      .send({
        username: '',
        password: '',
      });

    console.log('Response status:', response.statusCode);
    console.log('Response body:', response.body);

    expect(response.statusCode).toBe(400);
    expect(response.body).toHaveProperty('error.errors', ['Invalid username / password combination']);
  });
  
});
