const express = require('express');
const app = express();

app.use(express.json());

const baseUrl = process.env.BASE_URL || 'https://axel.dcidev.id/api/v1'; // Ganti dengan base URL default yang sesuai


module.exports = { app, baseUrl };
